# SpeedTest-Sched
Test the network speed with time schedule (by default every 60 seconds and 60 times in total).

This is based on [speedtest-cli](https://github.com/sivel/speedtest-cli) and thanks to the author.

## How to use?
### Requirement
- Python3 installed
- Clone this repo or download `speedtest_sched.py` and `speedtest.py`

### Simplest way
Run the python file `speedtest_sched.py` in terminal.
```bash
python3 speedtest_sched.py
```
The results will be printed in terminal.

### Run in background
The following command can run the file in background and save the results in a specified file.
```bash
nohup python3 -u speedtest_sched.py 2>&1 > speedtest.YYMMDD-hhmm.log &
```
The file name is just an example. Please use different file names for different tests.

#### Check the output when running in background
```bash
tail -f speedtest.YYMMDD-hhmm.log
```

## Output
```
2019-04-03 13:55:27         //time started to run the test
Download:184.3765265870924  //download speed in Mbit/s
Upload:171.84767105673953   //upload speed in Mbit/s
Ping:35.381                 //ping in ms
...
```

## Modify the code for different usage
### Change time interval and test counts
Change the values of `sched_span` and `count` at the beginning.

### Change test server
Change the value of `servers=[]`. Use an empty list will find the best server automatically.
The current server code is the nearest one to Kyudai.
#### Get a server list
Install [speedtest-cli](https://github.com/sivel/speedtest-cli) and use
```bash
speedtest-cli --list
```
The result is a list of servers by distance. The code at the beginning of each line can be used to set the server.

### Change output
Currently, only download / upload speed and ping are in the output. In fact, `results_dict` contains much more information.

## Reference
[speedtest-cli](https://github.com/sivel/speedtest-cli)
