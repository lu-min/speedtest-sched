#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Min Lu
# All Rights Reserved.

# To convert the output of speedtest_sched.py to csv

import numpy as np
from datetime import datetime, timedelta
# import matplotlib.pyplot as plt
# import matplotlib.patches as patches
# import matplotlib.image as mpimg

FILE_NAMES = []

class SpeedUnit(object):
    def __init__(self, date_time=datetime.now(), download=0, upload=0, ping=0):
        self.date = date_time.date()
        self.time = date_time.time()
        self.download = download
        self.upload = upload
        self.ping = ping

    def get_csvline(self):
        csvline = str(self.date) + ',' + str(self.time) + ',' + str(round(self.download, 2))  + ',' + str(round(self.upload, 2)) + ',' + str(round(self.ping))
        print(csvline)

def outputToCSV(dataArray, filename):
    with open(filename, 'w') as f:
        lines = []
        lines.append(dataArray[0]['Date'] + ',Download,Upload,Ping\n')
        for dataDict in dataArray:
            lines.append(dataDict['Time'] + ',' + str(dataDict['Download']) + ',' + str(dataDict['Upload']) + ',' + str(dataDict['Ping']) + '\n')
        f.writelines(lines)
    pass

if __name__ == "__main__":
    for filename in FILE_NAMES:
        dataArray = []
        with open(filename, 'r') as f:
            lines = f.readlines()
            count = len(lines)
            i = 0
            
            while count >= i + 4:
                try:
                    dataDict = {}
                    splitted = lines[i][:-1].split()
                    dataDict['Date'] = splitted[0]
                    dataDict['Time'] = splitted[1]
                    for k in range(1, 4):
                        splitted = lines[i+k][:-1].split(':')
                        dataDict[splitted[0]] = round(float(splitted[1]),2)
                    i += 4
                    dataArray.append(dataDict)
                except Exception:
                    print('End because of error')
                    break
        outputToCSV(dataArray, filename[:-3]+'csv')
    pass