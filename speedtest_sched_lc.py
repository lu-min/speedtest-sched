#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Min Lu
# All Rights Reserved.
# 
# Developed based on speedtest-cli
# https://github.com/sivel/speedtest-cli

import speedtest
import sched, time
import socket
import json

comments = "プログラミング演習（月3畑埜先生-2305）"
server_ip_addr = '13.231.178.152'
port_no = 60000
count = 90  # total count of tests
sched_span = 60 # time span of tests in seconds (start to run a test every sched_span seconds)

s = sched.scheduler(time.time, time.sleep)

# By Liu: upload the result to labcloud -- comment by Lu
def reporting(ip_addr,port,src_dict,start_time):
    s = socket.socket()             # Create a socket object
    host = socket.gethostname()     # Get local machine name
    json_dict = {}
    json_dict['source'] = host
    json_dict['source_ip_addr'] = socket.gethostbyname(host)
    json_dict['download'] = src_dict['download'] / 1024 / 1024
    json_dict['upload'] = src_dict['upload'] / 1024 / 1024
    json_dict['ping'] = src_dict['ping']
    json_dict['timestamp'] = time.time()
    json_dict['date'] = time.strftime("%Y-%m-%d", start_time)
    json_dict['time'] = time.strftime("%H:%M:%S", start_time)
    json_dict['comments'] = comments
    content = json.dumps(json_dict)
    print(content)
    buffer = bytearray(content,'utf-8')
    s.connect((ip_addr, port))
    s.send(buffer)
    s.close()
    print('connection closed')
    

def testSpeed(sc, num, span, num_max):
    if num >= num_max:
        return
    sc.enter(span, 1, testSpeed, (sc, num + 1, span, num_max))
    start_time = time.localtime(time.time())
    print(time.strftime("%Y-%m-%d %H:%M:%S", start_time))
    servers = [18709]   # can be changed
    # servers = [7976]
    # If you want to test against a server automatically selected (usually based on ping)
    # servers = []
    s = speedtest.Speedtest()
    s.get_servers(servers)
    s.get_best_server()
    s.download()
    s.upload()

    results_dict = s.results.dict()
    down = results_dict['download'] / 1024 / 1024
    up = results_dict['upload'] / 1024 / 1024
    print('Download:' + str(down))
    print('Upload:' + str(up))
    print('Ping:' + str(results_dict['ping']))
    try: 
        reporting(server_ip_addr,port_no,results_dict, start_time)
    except:
        print('Upload Module Error')
        
s.enter(1, 1, testSpeed, (s, 0, sched_span, count))
s.run()