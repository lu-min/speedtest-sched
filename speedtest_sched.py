#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Min Lu
# All Rights Reserved.
# 
# Developed based on speedtest-cli
# https://github.com/sivel/speedtest-cli

import speedtest
import sched, time

count = 60  # total count of tests
sched_span = 60 # time span of tests in seconds (start to run a test every sched_span seconds)

s = sched.scheduler(time.time, time.sleep)

def testSpeed(sc, num, span, num_max):
    if num >= num_max:
        return
    sc.enter(span, 1, testSpeed, (sc, num + 1, span, num_max))
    startTime = time.localtime(time.time())
    print(time.strftime("%Y-%m-%d %H:%M:%S", startTime))
    servers = [18709]   # can be changed
    # servers = [7976]
    # If you want to test against a server automatically selected (usually based on ping)
    # servers = []
    s = speedtest.Speedtest()
    s.get_servers(servers)
    s.get_best_server()
    s.download()
    s.upload()

    results_dict = s.results.dict()
    down = results_dict['download'] / 1024 / 1024
    up = results_dict['upload'] / 1024 / 1024
    print('Download:' + str(down))
    print('Upload:' + str(up))
    print('Ping:' + str(results_dict['ping']))
    print(results_dict)

s.enter(1, 1, testSpeed, (s, 0, sched_span, count))
s.run()